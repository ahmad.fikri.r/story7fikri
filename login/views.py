from django.shortcuts import render, redirect
from .forms import loginForm, registerForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

def login_view(request):
    form = loginForm()
    flag = False
    if request.method == "POST":
        form = loginForm(request.POST)
        if form.is_valid():
            uname = form.cleaned_data['username']
            passw = form.cleaned_data['password']
            user = authenticate(request, username=uname, password = passw)
            if user is not None:
                login(request, user)
                flag = True
                response = {'form':form,'user':user,'flag':flag}
                return redirect('/login')
            else:
                response = {'form':form,'user':user,'flag':flag}
                return redirect('/login')
    else:
        response = {'form':form,'flag':flag}
        return render(request,'loginPage.html',response)

def register_view(request):
    if request.user.is_authenticated:
	    return redirect ('/register')
    else:
        form = registerForm()
        if request.method == 'POST':
            form = registerForm(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(request, 'Account was created for ' + user)

                return redirect('/login')
			

        context = {'form':form}
        return render(request, 'registerPage.html', context)

def logout_view(request):
    logout(request)
    form = loginForm()
    response = {'form':form}
    return redirect('/login')

from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from .forms import loginForm,registerForm

class UnitTest(TestCase):
    def test_page_login(self):
        response = self.client.get('/login')
        self.assertEqual(response.status_code,200)
    
    def test_page_register(self):
        response = self.client.get('/register')
        self.assertEqual(response.status_code,200)

    def template_used_login(self):
        response = self.client.get(reverse('login:login'))
        self.assertTemplateUsed(response,'loginPage.html')

    def template_used_register(self):
        response = self.client.get(reverse('login:register'))
        self.assertTemplateUsed(response,'registerPage.html')

    def test_form_login_valid(self):
        form_login = loginForm(data={'username':"xxxx" , 'password':"abcdabcd"})
        self.assertTrue(form_login.is_valid())
        self.assertEqual(form_login.cleaned_data['username'],"xxxx")
        self.assertEqual(form_login.cleaned_data['password'],"abcdabcd")
        user = authenticate(username = "xxxx" , password = "abcdabcd")
        self.client.login()
        response = Client().get('/login')
        self.assertEqual(response.status_code,200)

    def test_form_register_valid(self):
        form_register = registerForm(data={
            'username':"xxxx" ,
            'email' : 'bbbbb@yahoo.com', 
            'password1':"abcdabcd",
            'password2':"abcdabcd"
        })
        self.assertTrue(form_register.is_valid())
        self.assertEqual(form_register.cleaned_data['username'],"xxxx")
        self.assertEqual(form_register.cleaned_data['email'],"bbbbb@yahoo.com")
        self.assertEqual(form_register.cleaned_data['password1'],"abcdabcd")
        self.assertEqual(form_register.cleaned_data['password2'],"abcdabcd")

    def test_logout_valid(self):
        self.client.logout
        response = Client().get('/login')
        form_login = loginForm()
        self.assertEqual(response.status_code,200)

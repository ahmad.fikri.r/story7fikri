from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.accordion, name='accordion'),
    path('searchbox/', views.searchbox, name='searchbox'),
    path('searchbox/data', views.data, name='data'),
    # dilanjutkan ...
]
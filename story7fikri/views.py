from django.shortcuts import render
from django.http import JsonResponse
import json
import requests

# Create your views here.

def accordion(request):
    return render(request, 'story7.html')

def searchbox(request):
    return render(request, 'searchbox.html')

def data(request):
    try:
        tes = request.GET['q']
    except:
        tes = "a"
    
    json_read = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + tes).json()

    return JsonResponse(json_read)

from django.test import TestCase, Client
from django.urls import reverse

# Create your tests here.

class UnitTest (TestCase):
    def test_url_200 (self):
        response = Client().get("/searchbox")
        self.assertEqual(response.status_code,200)

    def test_template (self):
        response = Client().get('/searchbox')
        self.assertTemplateUsed(response,'searchbox.html')

    def test_json_url(self):
        response = Client().get('/searchbox/data')
        self.assertEqual(response.status_code,200)

    def test_html_content (self):
        response = self.client.get(reverse('searchbox:searchbox'))
        isi = response.content.decode('utf8')
        self.assertIn("Book List" , isi)
        self.assertIn("No", isi)
        self.assertIn("Cover", isi)
        self.assertIn("Title", isi)
        self.assertIn("Author", isi)
        self.assertIn("Publisher", isi)
        self.assertIn("Published Date", isi)

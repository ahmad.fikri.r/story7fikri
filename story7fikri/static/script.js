$( function() {
    $( "#accordion" )
      .accordion({
        header: "> div > #accordion-header",
        collapsible: true,
        icons:false,
        heightStyle: "accordion-body",
        active: 0
      })
  } 
  );

  $( document ).ready(function() {
    setButtons();
    $(document).on('click', '.button-down', function(e) {
    var cCard = $(this).closest('.accordion-wrap');
    var tCard = cCard.next('.accordion-wrap');
    cCard.insertAfter(tCard);
    setButtons();
    });
    
    $(document).on('click', '.button-up', function(e) {
    var cCard = $(this).closest('.accordion-wrap');
    var tCard = cCard.prev('.accordion-wrap');
    cCard.insertBefore(tCard);
    setButtons();
    });
    
    function setButtons(){
        $('button').show();
        $('.accordion-wrap:last-child  button.down').hide();
        $('.accordion-wrap:first-child  button.up').hide();
    }
    });
